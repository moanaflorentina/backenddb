import {UserController} from "./controller/UserController";

export const Routes = [
    { 
    method: "get", 
    route: "/users", 
    controller: UserController, 
    action: "all" 
 },
    {
    method: "put",
    route: "/users/:id",
    controller: UserController,
    action: "update"
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one"
}, {
    method: "post",
    route: "/register",
    controller: UserController,
    action: "save"
}, {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove"
},
{
    method: "post",
    route: "/login",
    controller: UserController,
    action: "login"
}];