import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { Routes } from "./routes";
var cors = require("cors");
import * as jwt from "jsonwebtoken";

createConnection()
  .then(async (connection) => {
    // create express app
    const app = express();
    app.use(bodyParser.json());
    app.use(cors());

    app.use(function(req,res,next){
        verifyToken(req,res,next);
        console.log("Token verified");
    });

    // register express routes from defined application routes
    Routes.forEach((route) => {
      (app as any)[route.method](
        route.route,
        (req: Request, res: Response, next: Function) => {
          const result = new (route.controller as any)()[route.action](
            req,
            res,
            next
          );
          if (result instanceof Promise) {
            result.then((result) =>{
                console.log("result este ", result);
                result !== null && result !== undefined
                ? res.send(result)
                : undefined 
            });
          } else if (result !== null && result !== undefined) {
            res.json(result);
          }
        }
      );
    });

    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    // insert new users for test
    // await connection.manager.save(connection.manager.create(User, {
    //     email: "ionut23@yahoo.com",
    //     userName: "ionut23",
    //     password: "parola"
    // }));
    // await connection.manager.save(connection.manager.create(User, {
    //     email: "catalina8@gmail.com",
    //     userName: "catalina8",
    //     password: "parola"
    // }));

    console.log(
      "Express server has started on port 3000. Open http://localhost:3000/users to see results"
    );
  })
  .catch((error) => console.log(error));

function verifyToken(req: Request, res: Response, next: Function) {
  if (req.path === "/login" || req.path === "/register") {
    next();
    return;
  }

  let authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  console.log(token);
  if (token == null){
      res.status(401).send("Something wrong with token");
      return;
  } 

  jwt.verify(token, "secret", function(err) {
    if (err){
        console.log(err);
        res.status(403).send("Invalid token");
        return;    
    };
    next();
    return;
  });
}
