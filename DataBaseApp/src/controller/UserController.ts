import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";
var jwt = require("jsonwebtoken");

export class UserController {
  private userRepository = getRepository(User);

  async all(request: Request, response: Response, next: NextFunction) {
    console.log(this.userRepository.find());
    return this.userRepository.find();
  }

  async update(req: Request, res: Response, next: NextFunction) {
    const user = await this.userRepository.findOne(req.params.id);
    this.userRepository.merge(user, req.body);
    await this.userRepository.save(user);
    res.send(user);
  }

  async one(request: Request, response: Response, next: NextFunction) {
    return this.userRepository.findOne(request.params.id);
  }

  async remove(request: Request, response: Response, next: NextFunction) {
    await this.userRepository.delete(request.params.id);
    response.send(request.params.id);
  }

  async save(request: Request, response: Response, next: NextFunction) {
    const { email, userName, password } = request.body;
    if (!(email && password && userName)) {
      response.status(400).send("All input is required");
      return;
    }
    const existingUser = await this.userRepository.findOne({ email });
    if (existingUser) {
      response.status(409).send("User Already Exist. Please Login");
      return;
    }

    return this.userRepository.save({
      email,
      userName,
      password,
    });
  }

  async login(req: Request, res: Response, next: NextFunction) {
    try {
      const { email, password } = req.body;

      // Validate user input
      if (!(email && password)) {
        res.status(400).send("All input is required");
      }

      // Validate if user exist in out database
      const user = await this.userRepository.findOne({ email });
      if (user && password == user.password) {
        // Create token
        const token = jwt.sign(
          { user_id: user.id, email },
          "secret",
          {
            expiresIn: "2h",
          }
        );

        // save user token
        user.token = token;

        // user
        res.status(200).json(user);
        return;
      }
      res.status(400).send("Invalid Credentials");
    } catch (err) {
      console.log(err);
    }
  }
}
