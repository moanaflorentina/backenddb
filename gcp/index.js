const { Datastore } = require("@google-cloud/datastore");
const datastore = new Datastore({
  projectId: "fsd-project-334314",
  keyFilename: "./credentials.json",
});

const kindName = "schedule-log";

exports.getSchedule = async (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');

  if (req.method === 'OPTIONS') {
    // Send response to OPTIONS requests
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.set('Access-Control-Max-Age', '3600');
    res.status(204).send('');
  } else {
    const query = datastore.createQuery(kindName).order('start_date');
    const [tasks] = await datastore.runQuery(query);
    res.jsonp(tasks||[]);
  }
 
};

const isDate = (date) => {
  return new Date(date) !== "Invalid Date" && !isNaN(new Date(date));
};

exports.createSchedule = async (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');

  if (req.method === 'OPTIONS') {
    // Send response to OPTIONS requests
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.set('Access-Control-Max-Age', '3600');
    res.status(204).send('');
  } else {
    const schedule = req.body;
    try {
      if (
        !schedule ||
        !isDate(schedule.start_date) ||
        !isDate(schedule.end_date)
      ) {
        throw new Error("Invalid date format");
      }
  
      await datastore.save({
        key: datastore.key(kindName),
        data: {
          start_date: new Date(schedule.start_date),
          end_date: new Date(schedule.end_date),
        },
      });
      res.status(201).send("Successfully saved!");
    } catch (e) {
      console.error(e);
      res.status(500).send("Failed to save!");
    }
  
  }
 
 
};
